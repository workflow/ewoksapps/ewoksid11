# CHANGELOG.md

## (unreleased)

## 0.1.0

New Features:

- Calculate the Pair Distribution Function from integrated diffraction data
- Average diffraction patterns