ewoksid11 |version|
===================

*ewoksid11* provides data processing workflows for ID11.

*ewoksid11* has been developed by the `Software group <http://www.esrf.eu/Instrumentation/software>`_
and the ID11 staff of the `European Synchrotron <https://www.esrf.eu/>`_.

.. toctree::
    :hidden:

    api
